#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define QUALITY_LEVEL_1 5  // 1 packet each 5
#define QUALITY_LEVEL_2 4  // 1 packet each 4
#define QUALITY_LEVEL_3 3  // 1 packet each 3
#define QUALITY_LEVEL_4 2  // 1 packet each 2
#define QUALITY_LEVEL_5 1  // 1 packet each 1 (every packet)

void stampa(uint8_t p[], int l) {
  for (int i = 0; i < l; i++) {
    printf("%d ", p[i]);
  }
  printf("\n");
}

void compress(uint8_t** compressed, int compressed_length, uint8_t* samples,
              int* sample_length, int rate) {
  for (int i = 0, j = 0; i < compressed_length && (*sample_length) > 0; i++) {
    (*compressed)[i] = samples[j];
    j += rate;
    if (rate > (*sample_length))
      (*sample_length) = 0;
    else
      (*sample_length) -= rate;
  }
}

void decompress(uint8_t** decompressed, int* decompressed_length,
                uint8_t* compressed, int compressed_length, int rate) {
  *decompressed_length = compressed_length * rate;
  *decompressed = calloc(*decompressed_length, sizeof(uint8_t));
  for (int i = 0, k = 0; i < compressed_length; i++, k += rate) {
    (*decompressed)[k] = compressed[i];
  }
}

int main(int argc, char** argv) {
  if (argc < 2) return 1;
  int quality = atoi(argv[1]);
  printf("Quality: %d\n", quality);
  int rate;
  switch (quality) {
    case 1:
      rate = QUALITY_LEVEL_1;
      break;
    case 2:
      rate = QUALITY_LEVEL_2;
      break;
    case 3:
      rate = QUALITY_LEVEL_3;
      break;
    case 4:
      rate = QUALITY_LEVEL_4;
      break;
    case 5:
      rate = QUALITY_LEVEL_5;
      break;
    default:
      printf("Invalid quality value");
      exit(1);
      break;
  }
  int length = 52;
  uint8_t* samples = malloc(length);
  uint8_t* head = samples;
  int packet_length = 16;
  uint8_t* packet = calloc(packet_length, sizeof(uint8_t));

  printf("Original sample:\n");
  for (int i = 0; i < length; i++) {
    printf("%d ", i);
    samples[i] = i;
  }
  printf("\n");

  printf("Compression level: %.3f\%\n", 100 * (1 - (1 / (float)rate)));
  printf("Compressed: \n");
  compress(&packet, packet_length, samples, &length, rate);
  stampa(packet, packet_length);
  free(head);
  printf("Decompressed: \n");
  uint8_t* decompressed;
  int decompressed_length;
  decompress(&decompressed, &decompressed_length, packet, packet_length, rate);
  stampa(decompressed, decompressed_length);
  free(decompressed);
  free(packet);
}
