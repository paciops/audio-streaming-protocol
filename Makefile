# Skeleton Computer networks, Leiden University

CC      ?= gcc
SRC      = src
OBJS     = obj

WARNINGS = -Wall -Wextra -Wno-unused-variable -pedantic -g
IDIRS    = -I$(SRC)
LDIRS    =  -lm -lasound -pthread
CFLAGS   = $(IDIRS) -std=gnu99 $(WARNINGS) $(LDIRS)

find = $(shell find $1 -type f ! -path $3 -name $2 -print 2>/dev/null)

SERVERSRCS := $(call find, $(SRC)/, "*.c", "*/client/*")
CLIENTSRCS := $(call find, $(SRC)/, "*.c", "*/server/*")
SERVEROBJECTS := $(SERVERSRCS:%.c=$(OBJS)/%.o)
CLIENTOBJECTS := $(CLIENTSRCS:%.c=$(OBJS)/%.o)

ARCHIVE = assignment_2

all: server client

server: $(SERVEROBJECTS)
	$(CC) $(SERVEROBJECTS) -o exe_$@ $(CFLAGS)

client: $(CLIENTOBJECTS)
	$(CC) $(CLIENTOBJECTS) -o exe_$@ $(CFLAGS)

$(OBJS)/%.o: %.c
	@$(call echo,$(CYAN)Compiling $<)
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -o $@ -c $<

archive: clean
	tar -czf $(ARCHIVE).tar.gz --exclude ".git" .

clean:
	@echo Cleaning...
	@rm -rf $(OBJS) exe_server exe_client $(ARCHIVE).tar.gz vgcore.* valgrind_output.txt*
	@echo Done!

c: clean

.PHONY: c clean
