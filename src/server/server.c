/*
 * Skeleton-code behorende bij het college Netwerken, opleiding Informatica,
 * Universiteit Leiden.
 */

#include <fcntl.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include "../communication/asp/asp.h"
#include "../communication/com/common.h"
#include "../communication/packet/packet.h"

#define UNRELIABLE true
#define SLEEP_TIME 0.010  // seconds of the delay,  probably between 1 and 10 ms
#define DELAY_PROB 10     // probability of delay
#define LOSS_PROB 5       // probability of packet loss
#define CORRUPTION_PROB 10  // probability of packet corruption

#ifndef BIND_PORT
#define BIND_PORT 1234
#endif

#define VERBOSE false

struct wave_header {
  char riff_id[4];
  uint32_t size;
  char wave_id[4];
  char format_id[4];
  uint32_t format_size;
  uint16_t w_format_tag;
  uint16_t n_channels;
  uint32_t n_samples_per_sec;
  uint32_t n_avg_bytes_per_sec;
  uint16_t n_block_align;
  uint16_t w_bits_per_sample;
};

// wave file handle
struct wave_file {
  struct wave_header *wh;
  int fd;

  void *data;
  uint32_t data_size;

  uint8_t *samples;
  uint32_t payload_size;
};

static struct wave_file wf = {
    0,
};

// open the wave file
static int open_wave_file(struct wave_file *wf, const char *filename) {
  // Open the file for read only access
  wf->fd = open(filename, O_RDONLY);
  if (wf->fd < 0) {
    fprintf(stderr, "couldn't open %s\n", filename);
    return -1;
  }

  struct stat statbuf;
  // Get the size of the file
  if (fstat(wf->fd, &statbuf) < 0) return -1;

  wf->data_size = statbuf.st_size;  // Total size of the file

  // Map the file into memory
  wf->data = mmap(0x0, wf->data_size, PROT_READ, MAP_SHARED, wf->fd, 0);
  if (wf->data == MAP_FAILED) {
    fprintf(stderr, "mmap failed\n");
    return -1;
  }

  wf->wh = wf->data;

  // Check whether the file is a wave file
  if (strncmp(wf->wh->riff_id, "RIFF", 4) ||
      strncmp(wf->wh->wave_id, "WAVE", 4) ||
      strncmp(wf->wh->format_id, "fmt", 3)) {
    fprintf(stderr, "%s is not a valid wave file\n", filename);
    return -1;
  }

  // Skip to actual data fragment
  uint8_t *p = (uint8_t *)wf->data + wf->wh->format_size + 16 + 4;
  uint32_t *size = (uint32_t *)(p + 4);
  do {
    if (strncmp((char *)p, "data", 4)) break;

    wf->samples = p;
    wf->payload_size = *size;
    p += 8 + *size;
  } while (strncmp((char *)p, "data", 4) &&
           (uint32_t)(((uint8_t *)p) - (uint8_t *)wf->data) < statbuf.st_size);

  if (wf->wh->w_bits_per_sample != 16) {
    fprintf(stderr, "can't play sample with bitsize %d\n",
            wf->wh->w_bits_per_sample);
    return -1;
  }

  float playlength =
      (float)*size / (wf->wh->n_channels * wf->wh->n_samples_per_sec *
                      wf->wh->w_bits_per_sample / 8);

  printf("file %s, mode %s, samplerate %u, time %.1f sec\n", filename,
         wf->wh->n_channels == 2 ? "Stereo" : "Mono", wf->wh->n_samples_per_sec,
         playlength);
  return 0;
}

// close the wave file/clean up
static void close_wave_file(struct wave_file *wf) {
  munmap(wf->data, wf->data_size);
  close(wf->fd);
}

/*
Receive buffer dimension
*/
uint32_t receive_buffer(asp_socket_t *asp_socket) {
  uint32_t buffer_size;
  int n = recvfrom(asp_socket->sockfd, &buffer_size, sizeof(buffer_size),
                   MSG_WAITALL, (struct sockaddr *)&asp_socket->remote_addr,
                   &asp_socket->remote_addrlen);
  if (n == -1)
    pdie("Receive buffer dimension");
  else if (n == 0)
    printf("Peer has performed an orderly shutdown in receive_buffer.\n");
  return buffer_size;
}

/*Send sample size*/
void send_sample_size(asp_socket_t *asp_socket, uint32_t payload_size) {
  if (sendto(asp_socket->sockfd, &payload_size, sizeof(payload_size),
             MSG_CONFIRM, (const struct sockaddr *)&asp_socket->remote_addr,
             asp_socket->remote_addrlen) < 0)
    pdie("Send samples size");
}

/*Send all samples*/
void send_samples(asp_socket_t *asp_socket, uint32_t payload_size,
                  uint8_t *samples, uint32_t buffer_size) {
  uint32_t count = 0;
  uint32_t remaining_samples = payload_size;
  uint8_t *readhead = samples;
  packet_t to_send = {0};
  srand(time(NULL));
  bool keep_going = true;
  bool end = false;
  while (keep_going) {
    bool not_okay = true;
    uint32_t sent = 0;
    while (not_okay) {
      sent = packet_init(&to_send, readhead, count, buffer_size,
                         asp_socket->current_quality_level, remaining_samples);
      uint32_t data_length = get_packet_size(buffer_size);
      uint8_t *data = convert_to_data(&to_send, data_length, buffer_size);
      bool skip_packet = false;
      int n;
      if (UNRELIABLE) {
        // set the level
        uint32_t r = rand() % 100;
        if (r < LOSS_PROB) {
          skip_packet = true;
          if (VERBOSE) printf("Skipping packet #%d.\n", count);
        } else {
          if (r < DELAY_PROB) {
            if (VERBOSE)
              printf("Packet #%d delayed of %.3f seconds.\n", count,
                     SLEEP_TIME);
            sleep(SLEEP_TIME);
          }
          if (r < CORRUPTION_PROB) {
            int k = rand() % data_length;
            for (int i = 0; i < k; i++) data[rand() % data_length]++;
            if (VERBOSE) printf("Corrupting packet #%d.\n", count);
          }
        }
      }
      if (!skip_packet) {
        if (VERBOSE) printf("Sending packet #%d.\n", count);
        n = sendto(asp_socket->sockfd, data, data_length, MSG_CONFIRM,
                   (const struct sockaddr *)&asp_socket->remote_addr,
                   asp_socket->remote_addrlen);
        if (n < 0) {
          free(data);
          packet_reset(&to_send);
          pdie("Send samples");
        }
      }
      // response is going to be the command that the client send to the
      // server
      response_t response;
      n = recvfrom(asp_socket->sockfd, &response, sizeof(response), MSG_WAITALL,
                   (struct sockaddr *)&asp_socket->remote_addr,
                   &asp_socket->remote_addrlen);
      if (n == -1) {
        free(data);
        packet_reset(&to_send);
        pdie("Receive buffer dimension");
      } else if (n == 0) {
        free(data);
        packet_reset(&to_send);
        pdie("Peer has performed an orderly shutdown in receive_buffer");
      }
      not_okay = false;
      switch (response) {
        case PACKET_CORRUPTED:
          not_okay = true;
          if (VERBOSE) printf("Sending again #%d\n", count);
          break;
        case OK:
          // not_okay = false;
          // if (last_one) {
          //   if (VERBOSE)
          //     printf("Client wants packets but last one is already sent.\n");
          //   not_okay = true;
          // }
          break;
        case CLOSE_CONNECTION:
          // if (!last_one) {
          // }
          end = true;
          // not_okay = false;
          break;
        case QUALITY_INCREASE:
          asp_socket->current_quality_level += 1;
          break;
        case QUALITY_DECREASE:
          asp_socket->current_quality_level -= 1;
          break;
        default:
          printf("response_t not implemented\n");
          break;
      }
      free(data);
      packet_reset(&to_send);
    }
    count++;
    remaining_samples -= sent;
    readhead += sent;
    keep_going = (remaining_samples > 0) && !end;
  }
  if (VERBOSE) printf("No more bytes to send.\n");
}

int main(int argc, char **argv) {
  // Parse command-line options
  char *filename;
  int bind_port = BIND_PORT;
  if (VERBOSE) printf("Number of arguments: %d\n", argc);

  if (argc < 2) {
    printf("Usage: %s wav_file.wav\n", argv[0]);
    exit(1);
  }

  filename = argv[1];
  printf("Audio file name: %s\n", filename);

  // Open the WAVE file
  if (open_wave_file(&wf, filename) < 0) return -1;

  if (VERBOSE) printf("Sample size: %d\n", wf.payload_size);

  asp_socket_t asp_socket = init_connection(bind_port);
  // Bind the socket with the server address
  if (bind(asp_socket.sockfd, (const struct sockaddr *)&asp_socket.local_addr,
           asp_socket.local_addrlen) < 0) {
    pdie("Bind");
  }
  uint32_t buffer_size = receive_buffer(&asp_socket);
  if (VERBOSE) printf("Client wants buffer of %d bytes\n", buffer_size);
  send_sample_size(&asp_socket, wf.payload_size);

  send_samples(&asp_socket, wf.payload_size, wf.samples, buffer_size);

  // Clean up
  close_wave_file(&wf);
  close_connection(&asp_socket);
  return 0;
}
