#ifndef ASP_H
#define ASP_H
#include <netinet/in.h>
#include <stdint.h>
#include <sys/socket.h>
#include <sys/time.h>

#include "../packet/packet.h"

/* An asp socket descriptor for information about the sockets current state */
typedef struct asp_socket {
  int sockfd;

  struct sockaddr_in local_addr;
  socklen_t local_addrlen;

  struct sockaddr_in remote_addr;
  socklen_t remote_addrlen;

  struct timeval last_server_contact;
  struct timeval last_error;
  uint8_t current_quality_level;

  int errors;
  int packets_received;
  int packets_missing;
  uint32_t last_packet;
  int total_packets;
} asp_socket_t;

/*
Initialize the connect on port and if specified set the address
*/
asp_socket_t init_connection(int port);
/*
Close the socket of asp_socket
*/
void close_connection(const asp_socket_t* asp_socket);

#endif