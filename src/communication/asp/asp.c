/*
 * Skeleton-code behorende bij het college Netwerken, opleiding Informatica,
 * Universiteit Leiden.
 */
#include "asp.h"

#include <arpa/inet.h>
#include <math.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

#include "../com/common.h"

void close_connection(const asp_socket_t* asp_scoket) {
  close(asp_scoket->sockfd);
}

asp_socket_t init_connection(int port) {
  asp_socket_t asp_socket = {0};

  // Creating socket file descriptor
  if ((asp_socket.sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    pdie("Socket creation");
  }

  asp_socket.local_addrlen = sizeof(asp_socket.local_addr);
  asp_socket.remote_addrlen = sizeof(asp_socket.remote_addr);

  memset(&asp_socket.local_addr, 0, asp_socket.local_addrlen);
  memset(&asp_socket.remote_addr, 0, asp_socket.remote_addrlen);

  asp_socket.local_addr.sin_family = AF_INET;  // IPv4
  asp_socket.local_addr.sin_addr.s_addr = INADDR_ANY;
  asp_socket.local_addr.sin_port = htons(port);
  // quality level is set to the max
  asp_socket.current_quality_level = 5;
  return asp_socket;
}