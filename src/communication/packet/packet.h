#ifndef PACKET_H
#define PACKET_H
#include <stdint.h>

typedef struct packet {
  uint8_t* samples;
  uint32_t position;
  uint8_t last_one;
} packet_t;

typedef enum {
  OK,
  PACKET_CORRUPTED,
  QUALITY_INCREASE,
  QUALITY_DECREASE,
  CLOSE_CONNECTION
} response_t;

/*
Initialize a packet with position, last_one and samples
and return the number of samples inserted
*/
uint32_t packet_init(packet_t* const packet, uint8_t* samples,
                     uint32_t position, uint32_t buffer_size, uint8_t quality,
                     uint32_t remaining_samples);

/*
Reset a packet values to zero and free samples memory
*/
void packet_reset(packet_t* const packet);

/*
Convert packet to a buffer of uint8_t
*/
uint8_t* convert_to_data(const packet_t* const packet,
                         const uint32_t data_length, uint32_t buffer_size);

/*
Extract from data received the checksum, last_one, position and samples
*/
void convert_from_data(uint8_t* const, uint8_t* last_one, uint32_t* position,
                       uint8_t** samples);
/*
Get packet size
*/
uint32_t get_packet_size(uint32_t buffer_size);
#endif