#include "packet.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../com/common.h"

uint32_t packet_init(packet_t* const packet, uint8_t* samples,
                     uint32_t position, uint32_t buffer_size, uint8_t quality,
                     uint32_t remaining_samples) {
  uint32_t tot = remaining_samples;
  packet->position = position;
  packet->samples = calloc(buffer_size, sizeof(uint8_t));
  compress(&packet->samples, buffer_size, samples, &remaining_samples,
           get_rate(quality));
  packet->last_one = remaining_samples == 0;
  return tot - remaining_samples;
}
void packet_reset(packet_t* const packet) {
  packet->position = 0;
  free(packet->samples);
  packet->last_one = 0;
}

uint8_t* convert_to_data(const packet_t* const packet, uint32_t data_length,
                         uint32_t buffer_size) {
  uint8_t* const rawdata = malloc(data_length);
  uint8_t* writehead = rawdata;

  // skip this space for the checksum
  *(long*)writehead = 0;
  writehead += sizeof(long);

  *(uint8_t*)writehead = packet->last_one;
  writehead += sizeof(packet->last_one);
  *(uint32_t*)writehead = packet->position;
  writehead += sizeof(packet->position);
  if (memcpy(writehead, packet->samples, buffer_size) == NULL)
    pdie("Memory copy packet samples on data.");

  long sum = checksum(rawdata, data_length);
  writehead = rawdata;
  *(long*)writehead = sum;
  return rawdata;
}

void convert_from_data(uint8_t* const data, uint8_t* last_one,
                       uint32_t* position, uint8_t** samples) {
  uint8_t* head = data;
  // skip the checksum
  head += sizeof(long);

  *last_one = *(uint8_t*)head;
  head += sizeof(uint8_t);

  *position = *(uint32_t*)head;
  head += sizeof(uint32_t);

  *samples = head;
}

uint32_t get_packet_size(uint32_t buffer_size) {
  uint32_t result = 0;
  result += sizeof(long);      // checksum
  result += sizeof(uint8_t);   // last_one
  result += sizeof(uint32_t);  // position
  result += buffer_size;
  return result;
}