#ifndef COMMON_H
#define COMMON_H
#include <stddef.h>
#include <stdint.h>
#define QUALITY_LEVEL_1 5  // 1 packet each 5
#define QUALITY_LEVEL_2 4  // 1 packet each 4
#define QUALITY_LEVEL_3 3  // 1 packet each 3
#define QUALITY_LEVEL_4 2  // 1 packet each 2
#define QUALITY_LEVEL_5 1  // 1 packet each 1 (every packet)
/*
die and pdie are from this code
https://phoenix.goucher.edu/~kelliher/cs43/mar19.html
*/

/*
die --- Print a message and die.
*/
void die(const char*);

/*
Call perror() to figure out what's going on and die.
*/
void pdie(const char*);
/*
  Compute Internet Checksum for "count" bytes beginning at location "addr".
*/
long checksum(uint8_t* addr, size_t count);

void compress(uint8_t** const compressed, uint32_t compressed_length,
              const uint8_t* const samples, uint32_t* const sample_length,
              uint8_t rate);

void decompress(uint8_t** const decompressed,
                uint32_t* const decompressed_length,
                const uint8_t* const compressed, uint32_t compressed_length,
                uint8_t rate);
/*
Get rate of the samples to take according to quality
*/
uint8_t get_rate(uint8_t quality);

#endif