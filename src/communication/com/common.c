#include "common.h"

#include <stdio.h>
#include <stdlib.h>

void pdie(const char* mesg) {
  perror(mesg);
  exit(1);
}

void die(const char* mesg) {
  fputs(mesg, stderr);
  fputc('\n', stderr);
  exit(1);
}

long checksum(uint8_t* addr, size_t count) {
  register long sum = 0;

  while (count > 1) {
    /*  This is the inner loop */

    sum += *(uint16_t*)addr++;
    count -= 2;
  }
  /*  Add left-over byte, if any */
  if (count > 0) sum += *(uint8_t*)addr;

  /*  Fold 32-bit sum to 16 bits */
  while (sum >> 16) sum = (sum & 0xffff) + (sum >> 16);

  return ~sum;
}

void compress(uint8_t** const compressed, uint32_t compressed_length,
              const uint8_t* const samples, uint32_t* const sample_length,
              uint8_t rate) {
  for (uint32_t i = 0, j = 0; i < compressed_length && (*sample_length) > 0;
       i++) {
    (*compressed)[i] = samples[j];
    j += rate;
    if (rate > (*sample_length))
      (*sample_length) = 0;
    else
      (*sample_length) -= rate;
  }
}

void decompress(uint8_t** const decompressed,
                uint32_t* const decompressed_length,
                const uint8_t* const compressed, uint32_t compressed_length,
                uint8_t rate) {
  *decompressed_length = compressed_length * rate;
  *decompressed = calloc(*decompressed_length, sizeof(uint8_t));
  for (uint32_t i = 0, k = 0; i < compressed_length; i++, k += rate) {
    (*decompressed)[k] = compressed[i];
  }
}

uint8_t get_rate(uint8_t quality) {
  uint8_t rate;
  switch (quality) {
    case 1:
      rate = QUALITY_LEVEL_1;
      break;
    case 2:
      rate = QUALITY_LEVEL_2;
      break;
    case 3:
      rate = QUALITY_LEVEL_3;
      break;
    case 4:
      rate = QUALITY_LEVEL_4;
      break;
    case 5:
      rate = QUALITY_LEVEL_5;
      break;
    default:
      printf("Invalid quality value\n");
      rate = 1;
      break;
  }
  return rate;
}