/*
 * Skeleton-code behorende bij het college Netwerken, opleiding Informatica,
 * Universiteit Leiden.
 */

#include <alsa/asoundlib.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../communication/asp/asp.h"
#include "../communication/com/common.h"
#include "../communication/packet/packet.h"

#define TIMEOUT_TIME \
  50  // milliseconds that client waits before going in timeout
#define NUM_CHANNELS 2
#define SAMPLE_RATE 44100
// 1 Frame = Stereo 16 bit = 32 bit = 4kbit
#define FRAME_SIZE 4
#define PACKET_RESEND false
#define DECREASE_THRESHOLD 5   // in milliseconds
#define INCREASE_THRESHOLD 10  // in milliseconds

#ifndef BIND_PORT
#define BIND_PORT 1234
#endif

#define VERBOSE false

uint8_t *remaining_samples = NULL;
uint32_t remaining_samples_size = 0;

/*
Send buffer dimension to the server
*/
void send_buffer(asp_socket_t *asp_socket, uint32_t buffer_size) {
  if (sendto(asp_socket->sockfd, &buffer_size, sizeof(buffer_size), MSG_CONFIRM,
             (const struct sockaddr *)&asp_socket->local_addr,
             asp_socket->local_addrlen) < 0)
    pdie("Send buffer size");
}

/*
Get samples total dimension
*/
uint32_t receive_sample_size(asp_socket_t *asp_socket) {
  uint32_t sample_size;
  int n = recvfrom(asp_socket->sockfd, &sample_size, sizeof(sample_size),
                   MSG_WAITALL, (struct sockaddr *)&asp_socket->local_addr,
                   &asp_socket->local_addrlen);
  if (n == -1)
    pdie("Receive samples size");
  else if (n == 0)
    pdie("Peer has performed an orderly shutdown in receive_sample_size");
  if (sample_size < 1) pdie("Invalid sample size");
  printf("The samples will be %d byte long\n", sample_size);
  return sample_size;
}

/*Set timeout of 1 second if server stop sending packets*/
void set_timeout(const asp_socket_t *const asp_socket) {
  struct timeval tv = {0};
  tv.tv_usec = TIMEOUT_TIME * 1000;
  if (setsockopt(asp_socket->sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) <
      0) {
    pdie("Setsockopt");
  }
}

/*
Check if packet checksum is valid
*/
bool check_packet_integrity(uint8_t *data, uint32_t buffer_size) {
  // extract the checksum
  uint8_t *head = data;
  long checksum_server = *(long *)head;
  *(long *)head = 0;
  long checksum_client = checksum(data, get_packet_size(buffer_size));
  return checksum_client == checksum_server;
}

response_t adjust_quality(asp_socket_t *asp_socket, response_t response) {
  if (asp_socket->last_error.tv_sec != 0 &&
      asp_socket->last_error.tv_usec != 0) {
    struct timeval now;
    gettimeofday(&now, NULL);
    double milliseconds =
        ((now.tv_sec - asp_socket->last_error.tv_sec) * 1000000 +
         (now.tv_usec - asp_socket->last_error.tv_usec)) /
        1000;
    if (VERBOSE)
      printf("Time since last error: %.f\t Current quality: %d\n", milliseconds,
             asp_socket->current_quality_level);
    if (milliseconds < DECREASE_THRESHOLD &&
        asp_socket->current_quality_level > 1) {
      response = QUALITY_DECREASE;
      asp_socket->current_quality_level -= 1;
    } else if (milliseconds > INCREASE_THRESHOLD &&
               asp_socket->current_quality_level < 5) {
      response = QUALITY_INCREASE;
      asp_socket->current_quality_level += 1;
    }
  }
  return response;
}

/*Get sample from server*/
bool get_sample(asp_socket_t *asp_socket, uint8_t *to_play,
                uint32_t buffer_size) {
  uint32_t data_length = get_packet_size(buffer_size);
  uint8_t last_one;
  uint32_t position;
  uint8_t *samples;
  long sum;
  bool invalid_sample = false;
  uint8_t *raw_data = malloc(data_length);
  struct timeval now;
  do {
    // cycle until a sample received is valid
    if (raw_data == NULL) {
      pdie("Malloc raw_data");
    }
    int n = recvfrom(asp_socket->sockfd, raw_data, data_length, MSG_WAITALL,
                     (struct sockaddr *)&asp_socket->local_addr,
                     &asp_socket->local_addrlen);

    if (n == 0) {
      printf("Peer has performed an orderly shutdown in get_sample.\n");
      memset(to_play, 0, buffer_size);
      free(raw_data);
      return false;
    } else {
      response_t response = OK;
      if (n == -1) {
        last_one = 0;
        gettimeofday(&now, NULL);
        time_t delta = now.tv_sec - asp_socket->last_server_contact.tv_sec;
        if (delta > 3) {
          // server should be terminated so i close the connection
          last_one = 1;
          response = CLOSE_CONNECTION;
          printf("Closing ...\n");
        }
        invalid_sample = false;
        asp_socket->packets_missing++;
        memset(to_play, 0, buffer_size);
        gettimeofday(&asp_socket->last_error, NULL);
      } else {
        gettimeofday(&asp_socket->last_server_contact, NULL);
        if (check_packet_integrity(raw_data, buffer_size)) {
          // checksum is correct I extract every value
          convert_from_data(raw_data, &last_one, &position, &samples);
          // check if packet position is valid
          if (position < asp_socket->last_packet) {
            // packet is out of order
            // I can't play it
            // set sample to zeros
            memset(to_play, 0, buffer_size);
            asp_socket->packets_missing++;
            gettimeofday(&asp_socket->last_error, NULL);
          } else {
            // packet is in the correct order
            uint8_t *decompressed;
            uint32_t decompressed_length;
            decompress(&decompressed, &decompressed_length, samples,
                       buffer_size,
                       get_rate(asp_socket->current_quality_level));
            if (VERBOSE) printf("Packet #%d is valid.\n", position);
            asp_socket->packets_received++;
            asp_socket->last_packet = position;
            if (last_one)
              response = CLOSE_CONNECTION;
            else
              response = OK;
            // copying valid sample into the buffer to play
            memcpy(to_play, decompressed, buffer_size);
            if (buffer_size < decompressed_length) {
              remaining_samples_size = decompressed_length - buffer_size;
              remaining_samples = malloc(remaining_samples_size);
              memcpy(remaining_samples, decompressed + buffer_size,
                     remaining_samples_size);
            }
            free(decompressed);
          }
          invalid_sample = false;
        } else {
          // packet is corrupted
          asp_socket->errors++;
          gettimeofday(&asp_socket->last_error, NULL);
          if (PACKET_RESEND) {
            // ask to the server again for the packet
            invalid_sample = true;
            response = PACKET_CORRUPTED;
          } else {
            // set sample to zeros so the song is not a shit
            // but in this case samples is pointing to nothing
            invalid_sample = false;
            memset(to_play, 0, buffer_size);
            response = OK;
          }
          last_one = 0;
        }
      }
      if (!last_one) response = adjust_quality(asp_socket, response);
      int n =
          sendto(asp_socket->sockfd, &response, sizeof(response), MSG_CONFIRM,
                 (const struct sockaddr *)&asp_socket->local_addr,
                 asp_socket->local_addrlen);
      if (n < 0) pdie("Send response");
    }
  } while (invalid_sample);
  free(raw_data);
  // if last_one is one client doesn't need more samples
  return !last_one;
}
/*
Play samples using ALSA driver
*/
void play_music(int *i, snd_pcm_t *snd_handle, uint8_t *play_ptr,
                uint8_t *playbuffer, uint32_t buffer_size) {
  // write frames to ALSA

  snd_pcm_sframes_t frames =
      snd_pcm_writei(snd_handle, play_ptr,
                     (buffer_size - (*play_ptr - *playbuffer)) / FRAME_SIZE);
  // Check for errors
  int ret = 0;
  if (frames < 0) ret = snd_pcm_recover(snd_handle, frames, 0);
  if (ret < 0) {
    fprintf(stderr, "ERROR: Failed writing audio with snd_pcm_writei(): %i\n",
            ret);
    exit(EXIT_FAILURE);
  }
  if (frames > 0 &&
      frames < (buffer_size - (*play_ptr - *playbuffer)) / FRAME_SIZE)
    printf("Short write (expected %i, wrote %li)\n",
           (buffer_size - (*play_ptr - *playbuffer)) / FRAME_SIZE, frames);

  // advance pointers accordingly
  if (frames > 0) {
    play_ptr += frames * FRAME_SIZE;
    *i -= frames * FRAME_SIZE;
  }

  // if ((uint32_t)(*play_ptr - *playbuffer) == buffer_size) *i = 0;
}
/*
Prepare audio device
 */
snd_pcm_t *open_audio_device() {
  snd_pcm_t *snd_handle;

  int err = snd_pcm_open(&snd_handle, "default", SND_PCM_STREAM_PLAYBACK, 0);

  if (err < 0) {
    fprintf(stderr, "couldnt open audio device: %s\n", snd_strerror(err));
    pdie("snd_pcm_open");
  }

  // Configure parameters of PCM output
  err = snd_pcm_set_params(snd_handle, SND_PCM_FORMAT_S16_LE,
                           SND_PCM_ACCESS_RW_INTERLEAVED, NUM_CHANNELS,
                           SAMPLE_RATE,
                           0,        // Allow software resampling
                           500000);  // 0.5 seconds latency
  if (err < 0) {
    printf("couldnt configure audio device: %s\n", snd_strerror(err));
    pdie("snd_pcm_set_params");
  }
  return snd_handle;
}

/*
Print usage of the program
*/
void print_usage(const char *const name) {
  printf("Usage: %s -b buffer_size -p port (default: %d)\n", name, BIND_PORT);
  die("");
}

int main(int argc, char **argv) {
  uint16_t bind_port = BIND_PORT;
  char *p, *b;
  uint32_t buffer_size = 0;

  if (argc < 2) print_usage(argv[0]);
  for (int i = 1; i < argc; i += 2) {
    if (strcmp(argv[i], "-b") == 0 && i + 1 < argc) {
      // set buffer
      int tmp = strtol(argv[i + 1], &p, 10);
      if (*p != '\0' || errno != 0 || tmp > 65400 || tmp < 1)
        die("Invalid buffer size, value must be > 0 and < 65400");
      buffer_size = (uint32_t)tmp;
    } else if (strcmp(argv[i], "-p") == 0 && i + 1 < argc) {
      // set port
      int tmp = strtol(argv[i + 1], &b, 10);
      if (*b != '\0' || errno != 0 || tmp < 1 || tmp > 65535)
        die("Invalid port number, value must be > 0 and < 65535");
      bind_port = (uint16_t)tmp;
    } else {
      // print usage
      print_usage(argv[0]);
    }
  }
  if (buffer_size == 0) print_usage(argv[0]);
  // Set up network connection
  asp_socket_t asp_socket = init_connection(bind_port);
  send_buffer(&asp_socket, buffer_size);
  uint32_t sample_size = receive_sample_size(&asp_socket);
  set_timeout(&asp_socket);

  asp_socket.packets_received = 0;
  asp_socket.packets_missing = 0;
  asp_socket.total_packets = (sample_size + (buffer_size / 2)) / buffer_size;
  asp_socket.errors = 0;
  asp_socket.last_packet = 0;
  gettimeofday(&asp_socket.last_server_contact, NULL);

  // Open audio device
  snd_pcm_t *snd_handle = open_audio_device();

  // set up buffers/queues
  uint8_t *recvbuffer = malloc(buffer_size);
  uint8_t *playbuffer = malloc(buffer_size);

  bool keep_playing = get_sample(&asp_socket, recvbuffer, buffer_size);

  // Play
  printf("playing...\n");

  int i = 0;
  uint8_t *recv_ptr = recvbuffer;
  uint8_t *play_ptr;
  uint32_t progress = buffer_size;
  printf("%.1f%%", 100 * progress / (float)sample_size);
  while (keep_playing) {
    if (i <= 0) {
      memcpy(playbuffer, recvbuffer, buffer_size);
      play_ptr = playbuffer;
      i = buffer_size;
    }

    play_music(&i, snd_handle, play_ptr, playbuffer, buffer_size);
    progress += buffer_size + remaining_samples_size;
    if (remaining_samples != NULL && remaining_samples_size != 0) {
      play_ptr = remaining_samples;
      i = remaining_samples_size;
      play_music(&i, snd_handle, play_ptr, remaining_samples,
                 remaining_samples_size);
      free(remaining_samples);
      remaining_samples = NULL;
      remaining_samples_size = 0;
    }

    keep_playing = get_sample(&asp_socket, recvbuffer, buffer_size) &&
                   progress != sample_size;
    printf("\r\r\r");
    printf("%.1f%%", 100 * progress / (float)sample_size);
  }

  // clean up
  free(recvbuffer);
  free(playbuffer);
  if (remaining_samples != NULL) {
    free(remaining_samples);
  }

  snd_pcm_drain(snd_handle);
  snd_pcm_hw_free(snd_handle);
  snd_pcm_close(snd_handle);
  printf("\nINFO:\nReceived: %d\nMissing: %d\nErrors: %d\n",
         asp_socket.packets_received, asp_socket.packets_missing,
         asp_socket.errors);
  close_connection(&asp_socket);
  return 0;
}
